from typing import Dict
from jinja2 import Environment, FileSystemLoader
from configuration import pdf_defaults, default_project
import pdfkit
from os import listdir, mkdir
from os.path import isfile, join, isdir, realpath, dirname
import hjson
import pyqrcode
import errno
from typing import List

env = Environment(
    loader=FileSystemLoader('./' + default_project + '/layouts'),
    extensions=['jinja2_markdown.MarkdownExtension'],
    # autoescape=select_autoescape(['html', 'xml']) - We don't care about injection, it's a feature.
)


def _render(content, page_name: str, layout: str="main_article.html"):
    print("Rendering %s using the %s layout." % (page_name, layout))

    # render QR codes for links:
    if "links" in content:
        generate_qr_links(content["links"], page_name)

    # add some information for loading resources
    content["project"] = default_project
    content["page_name"] = page_name
    content["resource_path"] = dirname(realpath(__file__)) + "/" + default_project

    # overwrite the PDF options if the page says so. Might be forced via command line lateron.
    pdf_defaults["page-size"] = content.get("intended_format", "A4")

    template = env.get_template(layout)
    text = template.render(content)
    with open('./' + default_project + '/output/%s' % page_name + '.html', "w") as file:
        file.write(text)

    content["resource_path"] = "http://127.0.0.1:5500"
    text = template.render(content)
    with open('./' + default_project + '/live/%s' % page_name + '.html', "w") as file:
        file.write(text)

    # wkhtmltopdf is not the right choice:
    # - it does not support modern CSS, no columns and other new CSS.
    #
    # pdfkit.from_string(text, './' + default_project + '/output/%s' % page_name + '.pdf', options=pdf_defaults)


def auto_layout(intended_format: Dict):
    paper_size = intended_format["paper_size"] if intended_format["paper_size"] else "A4"
    orientation = intended_format["orientation"] if intended_format["orientation"] else "portrait"
    layout = intended_format["layout"] if intended_format["layout"] else "main_article"

    return "%s_%s_%s.html" % (paper_size, orientation, layout)


def pages(desired_pages: List[str]=None):
    mypath = './' + default_project + '/pages'

    if not desired_pages:
        desired_pages = listdir(mypath)
    else:
        desired_pages = [p + ".hjson" for p in desired_pages]

    return [{"page": f.replace(".hjson", ""), "content": hjson.load(open(join(mypath, f)))}
             for f in desired_pages if isfile(join(mypath, f))]


def render(desired_pages: List[str]=None):
    for page in pages(desired_pages=desired_pages):
        _render(content=page["content"]["content"],
                layout=auto_layout(page["content"]["intended_format"]),
                page_name=page["page"])


def generate_qr_links(links, page_name):
    qr_path = './%s/images/%s/qr/' % (default_project, page_name)

    for link in links:

        # Link itself might be changed, so don't check and just be slower.
        # if isfile('%s/%s.png' % (qr_path, link["title"])):
        #     continue

        url = pyqrcode.create(link["url"])

        try:
            mkdir('./%s/images/%s/' % (default_project, page_name))
            mkdir(qr_path)
        except OSError as exc:  # Python >2.5
            if exc.errno == errno.EEXIST and isdir(qr_path):
                pass
            else:
                raise
        url.png('%s/%s.png' % (qr_path, link["title"]), scale=8)
