from functions import render
import sys

print("museuminfo - Rendering...")
print("")
# if no arguments are given, try to use the defaults everywhere and render everything.
# as a convention everything should render when you check in.

if len(sys.argv) > 1:
    some = sys.argv[1:len(sys.argv)]
    print("Rendering pages: %s" % some)
    print("")
    render(some)
else:
    print("Rendering all pages, this can take a while...")
    print("If you want to render a single page, you can do so doing this:")
    print("")
    print("python3 render.py [page_name, [...]]")
    print("")
    print("Example: ")
    print("python3 render.py c64")
    print("")
    render()

print("")
print("Rendered pages are in ./output/")
print("")
print("Thank you for using museuminfo.")
print("")
