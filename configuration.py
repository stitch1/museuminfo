import os

default_project = os.environ.get('MUSEUM_DEFAULT_PROJECT', 'awesomespace')

# pages can override this.
pdf_defaults = {
    'page-size': 'A4',
    'margin-top': '0.0in',
    'margin-right': '0.0in',
    'margin-bottom': '0.0in',
    'margin-left': '0.0in',
    'encoding': "UTF-8",
    'custom-header': [
        ('Accept-Encoding', 'gzip')
    ],
    'no-outline': None,
    'quiet': '',
}

