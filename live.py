from livereload import Server, shell

# todo: parameters for single page.

server = Server()
server.watch('awesomespace/pages/*.hjson', shell('python3 render.py', cwd='.'))
server.watch('awesomespace/css/*.css', shell('python3 render.py', cwd='.'))
server.watch('awesomespace/layouts/*.html', shell('python3 render.py', cwd='.'))
server.serve(root='awesomespace/')
